const {
    calculateHours,
    crashMeteorite,
    createMultiplier,
    createStorage,
    createCounter,
    letTimeout,
    varTimeout
} = require('./tasks');

describe('createCounter', () => {
    test('Возвращает 1 при первом вызове', () => {
        const counterFunc = createCounter();
        const actual = counterFunc();
        expect(actual).toBe(1);
    });
    test('Возвращает 2 при втором вызове', () => {
        const counterFunc = createCounter();
        counterFunc();
        const actual = counterFunc();
        expect(actual).toBe(2);
    });
    test('Возвращает 5 при пятом вызове', () => {
        const counterFunc = createCounter();
        counterFunc();
        counterFunc();
        counterFunc();
        counterFunc();

        const actual = counterFunc();
        expect(actual).toBe(5);
    });
});

describe('calculateHours', () => {
    test('считает время дня на Земле', () => {
        expect(calculateHours.onEarth(1)).toBe(24);
    });
    test('считает время дня на Луне', () => {
        expect(calculateHours.onMoon(1)).toBe(29.5);
    });
    test('считает время нескольких дней на Земле', () => {
        expect(calculateHours.onEarth(3)).toBe(72);
    });
    test('считает время нескольких дней на Луне', () => {
        expect(calculateHours.onMoon(3)).toBe(88.5);
    });
    test('последовательные вызовы работают корректно', () => {
        expect(calculateHours.onMoon(2)).toBe(59);
        expect(calculateHours.onEarth(1.5)).toBe(36);
    });
});

describe('crashMeteorite', () => {
    beforeAll(() => {
        crashMeteorite();
    });

    test('считает время дня на Земле после аварии метеорита', () => {
        expect(calculateHours.onEarth(4)).toBe(88);
    });
    test('считает время дня на Луне после аварии метеорита', () => {
        expect(calculateHours.onMoon(2)).toBe(59);
    });
    test('еще один метеорит время не меняет', () => {
        crashMeteorite();
        expect(calculateHours.onEarth(2)).toBe(44);
    });
});

describe('createMultiplier', () => {
    test('должен возвращать функцию', () => {
        expect(typeof createMultiplier(3)).toBe('function');
    });
    test('должен при первом вызове умножать на 8', () => {
        const func = createMultiplier(40);
        expect(func()).toBe(320);
    });
    test('должен при втором вызове умножать на 10', () => {
        const func = createMultiplier(5);
        func();
        expect(func()).toBe(50);
    });
    test('должен при третьем вызове умножать на 256', () => {
        const func = createMultiplier(1);
        func();
        func();
        expect(func()).toBe(256);
    });
    test('должен при седьмом вызове умножать на 8', () => {
        const func = createMultiplier(10);
        for (let i = 1; i < 7; i++) {
            func();
        }
        expect(func()).toBe(80);
    });
    test('должен работать с 0', () => {
        const func = createMultiplier(0);
        expect(func()).toBe(0);
        expect(func()).toBe(0);
        expect(func()).toBe(0);
    });
    test('функции, вернувшиеся из функции createMultiplier работают независимо', () => {
        const func1 = createMultiplier(1);
        const func2 = createMultiplier(5);
        func1();
        func2();
        func2();
        expect(func1()).toBe(10);
        expect(func2()).toBe(1280);
    });
});

describe('createStorage', () => {
    test('должен возвращать объект', () => {
        expect(typeof createStorage()).toBe('object')
    });
    test('должен выполнять функцию get', () => {
        const dataStorage = createStorage();
        expect(dataStorage.get()).toBeDefined()
    });
    test('должен выполнять функцию add', () => {
        const dataStorage = createStorage();
        dataStorage.add(1,2,3);
        expect(dataStorage.get()).toEqual([1,2,3]);
    });
    test('должен выполнять функцию add несколько раз', () => {
        const dataStorage = createStorage();
        dataStorage.add(1,2,3);
        dataStorage.add(2);
        dataStorage.add(4,5);
        expect(dataStorage.get()).toEqual([1,2,3,2,4,5]);
    });
    test('должен выполнять функцию add с разными типами данных', () => {
        const dataStorage = createStorage();
        dataStorage.add(1);
        dataStorage.add(true);
        dataStorage.add(null);
        dataStorage.add('lol');
        dataStorage.add([1,2]);
        dataStorage.add(undefined);
        expect(dataStorage.get()).toEqual([1, true, null, 'lol', [1,2], undefined]);
    });
    test('должен выполнять функцию clear', () => {
        const dataStorage = createStorage();
        dataStorage.add(1,2,3);
        dataStorage.clear();
        dataStorage.add('ds');
        expect(dataStorage.get()).toEqual(['ds']);
    });
    test('должен выполнять функцию remove', () => {
        const dataStorage = createStorage();
        dataStorage.add(1,2,3,4,5,6,7,3434,'ds');
        dataStorage.remove(2);
        expect(dataStorage.get()).toEqual([1,3,4,5,6,7,3434,'ds']);
    });
    test('remove не должен удалять элемент, если его не существует', () => {
        const dataStorage = createStorage();
        dataStorage.add(1,2,3,4,5,7,'ds');
        dataStorage.remove(10);
        expect(dataStorage.get()).toEqual([1,2,3,4,5,7,'ds']);
    });
    test('remove должен удалять первый элемент', () => {
        const dataStorage = createStorage();
        dataStorage.add(1,2,3,4,5,7,'ds');
        dataStorage.remove(1);
        expect(dataStorage.get()).toEqual([2,3,4,5,7,'ds']);
    });
    test('remove должен удалять последний элемент', () => {
        const dataStorage = createStorage();
        dataStorage.add(1,2,3,4,5,7,'ds');
        dataStorage.remove('ds');
        expect(dataStorage.get()).toEqual([1,2,3,4,5,7]);
    });
});

// describe('letTimeout', () => {
//     it('Должен вернуть [0 ... 10]', async () => {
//         const actual = letTimeout();
//         await new Promise(res => setTimeout(res, 1000));
//         expect(actual).toEqual([0,1,2,3,4,5,6,7,8,9,10]);
//     });
// });
//
// describe('varTimeout', () => {
//     it('Должен вернуть [0 ... 10]', async () => {
//         const actual = varTimeout();
//         await new Promise(res => setTimeout(res, 1000));
//         expect(actual).toEqual([0,1,2,3,4,5,6,7,8,9,10]);
//     });
// });
//
